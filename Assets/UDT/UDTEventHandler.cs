using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Vuforia;
using UnityEngine.UI;
using Mono.Data.Sqlite;
using System.Data;
using System.Text;
public class UDTEventHandler : MonoBehaviour, IUserDefinedTargetEventHandler
{
	public ImageTargetBehaviour ImageTargetTemplate;
	const int MAX_TARGETS = 100; //Cantidad maxima de instancias
	int m_TargetCounter;
    private string dbPath;
    public Camera ARCamera;
    public Camera Camera;
    private string nombre;
    public Button guardarbtn;
    public InputField nomb;
    private Canvas canvas, canvasNombre;
	public int LastTargetIndex {
        get { return (m_TargetCounter - 1) % MAX_TARGETS; }
    }

	UserDefinedTargetBuildingBehaviour m_TargetBuildingBehaviour;
    ObjectTracker m_ObjectTracker;
    UDTFrameQualityMeter m_FrameQualityMeter;
    Vuforia.DataSet m_UDT_DataSet;
    ImageTargetBuilder.FrameQuality m_FrameQuality = ImageTargetBuilder.FrameQuality.FRAME_QUALITY_NONE;


    void Start() {
        Debug.Log("Ubicacion: " +Application.persistentDataPath);
        dbPath = "URI=file:" + Application.persistentDataPath + "/Imagenes.db";
        nomb.gameObject.SetActive(false);
        guardarbtn.gameObject.SetActive(false);
		canvas = GameObject.Find("CanvasCamara").GetComponent<Canvas>();
        canvasNombre = GameObject.Find("CanvasNombre").GetComponent<Canvas>();
        canvasNombre.gameObject.SetActive(false);
		m_FrameQualityMeter = FindObjectOfType<UDTFrameQualityMeter> ();
		m_TargetBuildingBehaviour = GetComponent<UserDefinedTargetBuildingBehaviour>();
        
        if (m_TargetBuildingBehaviour) {
            m_TargetBuildingBehaviour.RegisterEventHandler(this);
        }
        CreateSchema();
    }

    public void OnInitialized() {
        m_ObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        if (m_ObjectTracker != null) {
            m_UDT_DataSet = m_ObjectTracker.CreateDataSet();
            m_ObjectTracker.ActivateDataSet(m_UDT_DataSet);
        }
    }

    public void OnFrameQualityChanged(ImageTargetBuilder.FrameQuality frameQuality) {
        //Debug.Log("Calidad cambiada: " + frameQuality.ToString());
        if(ARCamera.enabled){
            m_FrameQuality = frameQuality;
            if (m_FrameQuality == ImageTargetBuilder.FrameQuality.FRAME_QUALITY_LOW) {
            
            }

            m_FrameQualityMeter.SetQuality(frameQuality);
        }
    }

	//Se ejecuta cada vez que se crea una instancia
    public void OnNewTrackableSource(TrackableSource trackableSource) {
       

         m_TargetCounter++;
        m_ObjectTracker.DeactivateDataSet(m_UDT_DataSet);
		//Crea una instancia nueva y borra la mas vieja
        if (m_UDT_DataSet.HasReachedTrackableLimit() || m_UDT_DataSet.GetTrackables().Count() >= MAX_TARGETS) { 
			IEnumerable<Trackable> trackables = m_UDT_DataSet.GetTrackables();
            Trackable oldest = null;
            foreach (Trackable trackable in trackables) {
                if (oldest == null || trackable.ID < oldest.ID)
                    oldest = trackable;
                    Debug.Log("Array: " +oldest);
                    
            }

            if (oldest != null) {
                m_UDT_DataSet.Destroy(oldest, true);
            }
        }

        ImageTargetBehaviour imageTargetCopy = Instantiate(ImageTargetTemplate);
        imageTargetCopy.gameObject.name = nombre;
        
        m_UDT_DataSet.CreateTrackable(trackableSource, imageTargetCopy.gameObject);
        InsertImagen(nombre, trackableSource,imageTargetCopy);
        m_ObjectTracker.ActivateDataSet(m_UDT_DataSet);

        m_TargetBuildingBehaviour.StartScanning();
    }
	//Se llama a ventana nombre
    public void BuildNewTarget() {
         Guardar(ImageTargetTemplate);

        
    }

    //Crea nueva Instancia
    public void nuevaInstancia(ImageTargetBehaviour x){
        canvasNombre.gameObject.SetActive(false);
        canvas.gameObject.SetActive(true);
        
        if (m_FrameQuality == ImageTargetBuilder.FrameQuality.FRAME_QUALITY_MEDIUM ||
            m_FrameQuality == ImageTargetBuilder.FrameQuality.FRAME_QUALITY_HIGH) {
            
            
            m_TargetBuildingBehaviour.BuildNewTarget(nombre, x.GetSize().x);
            
        }
        else {
            Debug.Log("No se puede instanciar el objeto; poca calidad de Imagen");
        }
        ARCamera.enabled = true;
        Camera.enabled = false;
    }
    //Ventana nombre

    public void Guardar(ImageTargetBehaviour x){
        canvasNombre.gameObject.SetActive(true);
        canvas.gameObject.SetActive(false);
        ARCamera.enabled = false;
        Camera.enabled = true;
        nomb.gameObject.SetActive(true);
        guardarbtn.gameObject.SetActive(true);
        guardarbtn.onClick.AddListener(delegate {ObtenerNombre(x); });
    }

    public void ObtenerNombre(ImageTargetBehaviour x){
        nombre = nomb.text;
        nuevaInstancia(x);
    }
    //SQLite
    public void CreateSchema() {
			using (var conn = new SqliteConnection(dbPath)) {
				conn.Open();
				using (var cmd = conn.CreateCommand()) {
					cmd.CommandType = CommandType.Text;
					cmd.CommandText = "CREATE TABLE IF NOT EXISTS 'Targets' ( " +
					                  "  'id' INTEGER PRIMARY KEY, " +
					                  "  'nombre' TEXT NOT NULL, " +
					                  "  'trackable' BLOB NOT NULL, " +
                                       " 'gameobject' BLOB NOT NULL " +
					                  ");";

					var result = cmd.ExecuteNonQuery();
					Debug.Log("Base de datos creada: " + result);
				}
			}
		}

    public void InsertImagen(string nombre,TrackableSource trackable, ImageTargetBehaviour gameobject) {
			using (var conn = new SqliteConnection(dbPath)) {
				conn.Open();
				using (var cmd = conn.CreateCommand()) {
					cmd.CommandType = CommandType.Text;
					cmd.CommandText = "INSERT INTO Targets (nombre,trackable,gameobject) " +
					                  "VALUES (@Name,@Trackable,@GameObject);";

					cmd.Parameters.Add(new SqliteParameter {
						ParameterName = "Name",
						Value = nombre
					});
                    cmd.Parameters.Add(new SqliteParameter {
						ParameterName = "Trackable",
						Value = trackable
					});
                    cmd.Parameters.Add(new SqliteParameter {
						ParameterName = "GameObject",
						Value = gameobject
					});
					

					var result = cmd.ExecuteNonQuery();
					Debug.Log("imagen insertada: " + result);
                    //GetImagenes(10);
				}
			}
		}

   
}