﻿using UnityEngine;
using Vuforia;
using UnityEngine.UI;
public class UDTTrackableEventHandler : MonoBehaviour, ITrackableEventHandler {

	protected TrackableBehaviour mTrackableBehaviour;
	public Canvas canvas, canvas2,canvas3;
	public Text texto;
	public string info;
	public TextMesh textObject;
	public Camera ARCamera;
    private Camera Camera;
	protected virtual void Start () {
		textObject = GameObject.Find("TextoInformativo").GetComponent<TextMesh>();
		
		canvas3 = GameObject.Find("Canvas3").GetComponent<Canvas>();
		mTrackableBehaviour = GetComponent<TrackableBehaviour> ();
		if (mTrackableBehaviour)
			mTrackableBehaviour.RegisterTrackableEventHandler (this);
	}

	protected virtual void OnDestroy () {
		if (mTrackableBehaviour)
			mTrackableBehaviour.UnregisterTrackableEventHandler (this);
	}

	public void OnTrackableStateChanged (
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus) {
			
				if (newStatus == TrackableBehaviour.Status.DETECTED ||
					newStatus == TrackableBehaviour.Status.TRACKED ||
					newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
					Debug.Log ("Trackable " + mTrackableBehaviour.TrackableName + " found");
					OnTrackingFound (mTrackableBehaviour.TrackableName);
					canvas.enabled = false;
				} else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
						newStatus == TrackableBehaviour.Status.NO_POSE) {
					Debug.Log ("Trackable " + mTrackableBehaviour.TrackableName + " lost");
					OnTrackingLost ();
					canvas.enabled = true;
				} else {
					OnTrackingLost ();
					canvas.enabled = true;
				}
			
	}

	protected virtual void OnTrackingFound (string nombre) {
		var rendererComponents = GetComponentsInChildren<Renderer> (true);
		var colliderComponents = GetComponentsInChildren<Collider> (true);
		var canvasComponents = GetComponentsInChildren<Canvas> (true);
		Debug.Log("Objeto detectado: " + nombre);
		
		canvas3.enabled = true;
		texto.text = nombre;
		// Enable rendering:
		foreach (var component in rendererComponents)
			component.enabled = true;

		// Enable colliders:
		foreach (var component in colliderComponents)
			component.enabled = true;

		// Enable canvas':
		foreach (var component in canvasComponents)
			component.enabled = true;
			
	}


	protected virtual void OnTrackingLost () {
		Debug.Log("Objeto perdido");
		var rendererComponents = GetComponentsInChildren<Renderer> (true);
		var colliderComponents = GetComponentsInChildren<Collider> (true);
		var canvasComponents = GetComponentsInChildren<Canvas> (true);
		canvas3.enabled = false;
		
		// Disable rendering:
		foreach (var component in rendererComponents)
			component.enabled = false;

		// Disable colliders:
		foreach (var component in colliderComponents)
			component.enabled = false;

		// Disable canvas':
		foreach (var component in canvasComponents)
			component.enabled = false;
			texto.text ="";
	}

}
